!------------------------------------------------------------------------------!
! CODE NASA Breakup Model
!------------------------------------------------------------------------------!
!
!> \mainpage Implementation of the NASA Breakup Model 
!> @version 2.00 / 2019
!> @date 01.03.2019
!> @author Alexis Petit (alexis.petit@obspm.fr), IFAC-CNR, IMCCE
!> @section structure Structure
!> @image html classbreakupmodel_a59bacc178d070dc4929bcecb06fdf9c6_cgraph.png
!> @section sources Original papers
!!  Johnson et al., NASA's new breakup model of EVOLVE 4.1, Adv. Space Res. Vol. 8, No 9, pp.1377-1384, 2001
!
! DESCRIPTION: 
!> Program to create a cloud of space debris.
!
! REVISION HISTORY:
! 02 02 2018 - New documentation
! 02 02 2018 - Initial Version
!
!> @remark To create a documentation:
!>   $ doxygen -g doc
!>   $ doxygen doc
!
!------------------------------------------------------------------------------!

PROGRAM main
   
  USE breakupModel, ONLY : fragmentation
  USE coordinates,  ONLY : kepl_2_cart,cart_2_kepl,ma_2_ea
  USE constants

  IMPLICIT NONE

  !+*************************************************************************!
  !+*************************************************************************!
  !+                                                                         !
  !+                               VARIABLES                                 !
  !+                                                                         !
  !+*************************************************************************!
  !+*************************************************************************!

  !global variables coming from "population.f90"
  LOGICAL  ::      event_flag
  REAL(WP) ::      event_position(8)
  REAL(WP) ::      event_param(2)
  CHARACTER(60) :: event_type
  CHARACTER(60) :: event_name
  REAL(WP) :: kepl(6)

  !> @brief The size limit for space debris created
  REAL(WP) :: limit_size  
  !variables coming from "scheduler"
  !> @brief Id NORAD
  INTEGER :: id_norad
  !> @brief Date in Julian day of the event
  REAL(WP) :: jd_event
  !> @brief Date in YYYY,MM,DD
  REAL(WP) :: year,month,day
  !> @brief Type of event
  CHARACTER(60) :: event
  !> @brief Keplerian coordinates of the event
  REAL(WP) :: sma,ecc,inc,raan,omega,ma
  !> @brief Flag an error
  INTEGER :: error
  !> @brief Name of the parent body
  CHARACTER(30) :: name_object

  !variable coming from "update_population"
  !> @brief Final number of objects
  INTEGER :: new_nb_obj
  !> @brief Number of new objects
  INTEGER :: nb_debris_selected
  !> @brief Loop counter
  INTEGER :: k,l
  !> @brief Altitude of the object
  REAL(WP) :: altitude
  !> @brief Array containing the characteristics of the new debris
  REAL(WP) :: debris(1000000,6)
  !> @brief Number of debris with a size upper than the size limit   
  INTEGER :: nb_debris 
  !> @brief Cartesian coordinate of the parent body
  REAL(WP) :: cart(6)
  !> @brief Cartesian coordinate of a new debris
  REAL(WP) :: new_cart(6)
  !> @brief State vector 
  REAL(WP) :: state_vector(6)
  !> @brief Balistic coefficient (B=Cd*AM)
  REAL(WP) :: BC
  !> @brief Total mass of the debris
  REAL(WP) :: mass  
  !> @brief Array containing the debris created
  REAL(WP), DIMENSION(:,:), ALLOCATABLE :: debris_selected
  !> @brief Statut of the file
  CHARACTER(len=20) :: statut_file
  !> @brief Output format
  CHARACTER(len=60) :: FMT
  !> @brief Argument
  CHARACTER(len=3) :: argument
  !> @brief Line of the event
  INTEGER :: line
  !> @brief Name of the parameter file
  CHARACTER(len=50) :: name_file
  
  !+*************************************************************************!
  !+*************************************************************************!
  !+                                                                         !
  !+                               PROGRAM                                   !
  !+                                                                         !
  !+*************************************************************************!
  !+*************************************************************************!

  WRITE(*,'(A)') '> Program test of the NASA/Breakup Model (Johnson et al., 2001)'

  ! 
  ! FIRST PART: we read initial conditions
  !

  IF(iargc()==1) THEN
    CALL GETARG(1,argument)
    READ(argument,'(I3)') line
  ELSE
    line = 1
  END IF

  !Read the event characteristics
  OPEN(unit=21,file="inputs/schedule.txt",action="read",status="old",iostat=error)
  IF (error>0) THEN
    WRITE(*,*) 'Bad name for the scheduler file'
    STOP
  END IF
  DO k=1,line
     READ(21,*)
  END DO 

  READ(21,*) id_norad,jd_event,year,month,day,name_object,event,sma,ecc,inc, &
             raan,omega,ma,event_param(1:2)
  
  event_position(1) = sma
  event_position(2) = ecc
  event_position(3) = inc*PI/180.00
  event_position(4) = raan*PI/180.00
  event_position(5) = omega*PI/180.00
  event_position(6) = ma*PI/180.00
  event_type = event
  event_name = name_object
  
  ! 
  ! SECOND PART: we model the fragmentation
  !

  WRITE(*,'(A,I7)') '| Fragmentation of the object ',id_norad

  WRITE(name_file,'(I0)') int(id_norad)
  name_file = "inputs/nbm_param_"//TRIM(name_file)//".txt"
  CALL fragmentation(event_type,name_file,event_param(2),event_param(1),nb_debris,debris,mass,limit_size)

  WRITE(*,'(5A,I10,A,F10.3,A)') '| The fragmentation of ',TRIM(event_name), &
       ' (',TRIM(event_type),') generates ',nb_debris, &
       ' debris for a total mass of',mass,' kg ' 

  !Select debris with a size upper than 1 cm
  nb_debris_selected=0
  DO l=1,nb_debris
    IF (debris(l,1).gt.limit_size) THEN
      nb_debris_selected = nb_debris_selected + 1
    END IF
  END DO

  ALLOCATE(debris_selected(nb_debris_selected,11))
  
  !Parent body > debris
  CALL kepl_2_cart(event_position,earth_mu,cart)
  k = 0
  DO l=1,nb_debris 
    IF (debris(l,1).gt.limit_size) THEN
      new_cart(1:3) = cart(1:3)
      new_cart(4:6) = cart(4:6)+debris(l,3:5)
      k = k + 1
      debris_selected(k,1:6)  = new_cart(1:6)      !coordinate
      debris_selected(k,7)    = debris(l,2)        !A/M
      debris_selected(k,8)    = k                  !New id
      debris_selected(k,9)    = debris(l,1)        !Size
      debris_selected(k,10)   = debris(l,6)        !Mass
    END IF
  END DO

  !
  ! THIRD PART: write the initial conditions of the cloud of space debris
  !

  !------------------------------------------  
  !Generation of orbital elements (for NIMASTEP)
  OPEN(unit=95, file="outputs/cloud.txt",action="write",form="formatted")
  OPEN(unit=96, file="outputs/cloud_cart.txt",action="write",form="formatted")

  FMT = '(A13,A7,A40,A10,A13,A9,4A11,A12,A11,A11)'
  WRITE(95,FMT) 'Julian_Day','NORAD','Name','Alt.[km]','a[m]','e', &
                'i[deg]','RAAN[deg]','Omega[deg]','MA[deg]','BC[m2/kg]','Mass[kg]','Size[m]'
  FMT = '(F013.5,I07,A40,F10.3,F13.3,F9.6,4F11.4,F12.7,F11.5,F11.7)'

  FMT = '(A13,A7,A40,A10,A13,A9,4A11,A12,A14,A11)'
  WRITE(96,FMT) 'Julian_Day','NORAD','Name','Alt.[km]','x[m]','y[m]', &
                'z[m]','vx[m/s]','vy[m/s]','vz[m/s]','BC[m2/kg]','Mass[kg]','Size[m]'
  FMT = '(F013.5,I07,A40,F10.3,F13.3,F9.6,4F11.4,F12.7,F14.7,F11.7)'
  
  DO k=1,nb_debris_selected
    !WRITE(*,*) debris_selected(k,1:6) 
    altitude = (SQRT(DOT_PRODUCT(debris_selected(k,1:3), &
                 debris_selected(k,1:3)))-earth_eq)/1000.0
    CALL cart_2_kepl(debris_selected(k,1:6),earth_mu,kepl)    
    WRITE(95,FMT) jd_event,INT(debris_selected(k,8)),TRIM(event_name)//'_DEB', &
                  altitude,kepl(1),kepl(2),kepl(3:6)*180./PI, &
                  debris_selected(k,7)*2.2,debris_selected(k,10),debris_selected(k,9)
    !WRITE(*,*) kepl(1:2),kepl(3:6)*180._WP/PI
    
    WRITE(96,*) jd_event,INT(debris_selected(k,8)),TRIM(event_name)//'_DEB', &
                  altitude,debris_selected(k,1:6), &
                  debris_selected(k,7)*2.2,debris_selected(k,10),debris_selected(k,9)
    CALL kepl_2_cart(kepl,earth_mu,debris_selected(k,1:6))
    !WRITE(*,*) debris_selected(k,1:6)
    !WRITE(*,*) ''
  END DO
 
  CLOSE(95)
  CLOSE(96)
  
  OPEN(unit=95, file="outputs/cloud_dv.txt", action="write",  form="formatted")

  FMT = '(A7,4A10,A12)'
  WRITE(95,FMT) 'ID','Size[m]','DVx[m/s]','DVy[m/s]','DVz[m/s]','A/M[m2/kg]'
  FMT = '(I07,4F10.3,F12.4)'
  k = 0
  DO l=1,nb_debris
    IF (debris(l,1).gt.limit_size) THEN
      WRITE(95,FMT) k,debris(l,1),debris(l,3:5),debris(l,2)
      k = k + 1
    END IF
  END DO
  CLOSE(95)
  WRITE(*,'(A,F6.4,A,I6)') '| Number of fragment (>',limit_size,' m) : ', &
                           nb_debris_selected
  DEALLOCATE(debris_selected)
  
END PROGRAM main
