!==============================================================================!
! UNAMUR, NASA Breakup model of EVOLVE 4.0                                     !
!==============================================================================!
! MODULE: coordinates                                                         
!                                                                              
!> @file coordinates.f90                                                      
!> @author Alexis Petit, PhD Student, University of Namur                      
!> @contact alexis.petit@obspm.fr                                              
!> @brief This module performs coordinate transformations                          
!                                                                              
!==============================================================================!

MODULE coordinates

  !============================================================================!
  !MODULE contains all subroutine about the date                               !
  !                                                                            !
  ! - nu_2_ma                                                                  !
  ! - ea_2_nu                                                                  !
  ! - ma_2_ea                                                                  !
  ! - ea_2_ma                                                                  !
  ! - cart_2_kepl                                                              !
  ! - kepl_2_cart                                                              !
  ! - cart_2_sph                                                               !
  ! - sph_2_cart                                                               !
  ! - kepl_2_equinoc                                                           !
  !============================================================================!

  USE constants
  
  IMPLICIT NONE

CONTAINS

  !----------------------------------------------------------------------------!
  !> @brief The eccentric anomaly from the true anomaly (Vallado, 2012 p.77)
  !> @param[in] nu   true anomaly                                          
  !> @param[in] ecc  eccentricity                                          
  !> @param[out] ea  eccentric anomaly                                     
  !----------------------------------------------------------------------------!
  SUBROUTINE nu_2_ea(nu,ecc,ea)

    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: nu
    REAL(WP),INTENT(in) :: ecc
    REAL(WP),INTENT(out) :: ea
    
    !********************
    !  LA SUBROUTINE
    !********************

    IF (ecc.lt.0) THEN

      STOP 'Error (nu_2_ea subroutine) : the eccentricity can not be negative'

    ELSE IF (ecc.ge.1) THEN
      
      STOP 'Error (nu_2_ea subroutine) : hyperbolic case'

    ELSE

      ea = ASIN(SIN(nu)*SQRT(1-ecc**2)/(1+ecc*COS(nu)))

    END IF
      
  END SUBROUTINE nu_2_ea

  !----------------------------------------------------------------------------!
  !> @brief Compute the true anomaly from the eccentric anomaly            
  !> @param[in] ea   eccentric anomaly                                     
  !> @param[in] ecc  eccentricity                                          
  !> @param[out] nu  true anomaly                                          
  !----------------------------------------------------------------------------!
  SUBROUTINE ea_2_nu(ea,ecc,nu)

    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: ea
    REAL(WP),INTENT(in) :: ecc
    REAL(WP),INTENT(out) :: nu
    
    !********************
    !  LA SUBROUTINE
    !********************

    nu = 2*ATAN(SQRT((1+ecc)/(1-ecc))*TAN(ea/2))
   
  END SUBROUTINE ea_2_nu

  !----------------------------------------------------------------------------!
  !> @brief Compute the eccentric anomaly from the mean anomaly       
  !> @param[in] ma   mean anomaly                                        
  !> @param[in] ecc  eccentricity                                        
  !> @param[out] ea  eccentric anomaly                                   
  !----------------------------------------------------------------------------!
  SUBROUTINE ma_2_ea(ma,ecc,ea)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: ma
    REAL(WP),INTENT(in) :: ecc
    REAL(WP),INTENT(out) :: ea    

    REAL(WP) :: ea0
    REAL(WP),PARAMETER :: precision = 1D-8
    
    !********************
    !  LA SUBROUTINE
    !********************

    IF (ecc.lt.0) THEN

      STOP 'Error (ma_2_ea subroutine) : the eccentricity can not be negative'

    ELSE IF (ecc.ge.1) THEN
      
      STOP 'Error (ma_2_ea subroutine) : hyperbolic case'

    ELSE

      ! We solve Kepler's equation (Vallado, 2014, p.65)
       
      IF (((ma<0).and.(ma>-PI)).or.(ma>PI)) THEN
        ea0 = ma - ecc
      ELSE
        ea0 = ma + ecc
      END IF
      ea=1d6
      DO WHILE(ABS(ea-ea0)>precision)
        ea0 = ea 
        ea = ea0 + (ma-ea0+ecc*SIN(ea0))/(1-ecc*COS(ea0))
      END DO   

    END IF
   
  END SUBROUTINE ma_2_ea

  !----------------------------------------------------------------------------!
  !> @brief Give the mean anomaly from the eccentric anomaly
  !> @param[in] ea   eccentric anomaly                                    
  !> @param[in] ecc  eccentricity
  !> @param[out] ma  mean anomaly                                         
  !----------------------------------------------------------------------------!
  SUBROUTINE ea_2_ma(ea,ecc,ma)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: ea
    REAL(WP),INTENT(in) :: ecc
    REAL(WP),INTENT(out) :: ma

    !********************
    !  LA SUBROUTINE
    !********************

    ma = ea-ecc*sin(ea)
    ma = MOD(ma,2*PI)
    IF (ma<0) THEN
      ma = ma + 2*PI
    END IF

  END SUBROUTINE ea_2_ma

  !----------------------------------------------------------------------------!
  !> @brief Give the keplerian coordinates from cartesian coordinates
  !> @param[in] cart  cartesian coordinates                                 
  !> @param[in] mu    G*M                                                   
  !> @param[out] kepl keplerian coordinates                                
  !----------------------------------------------------------------------------!
  SUBROUTINE cart_2_kepl(cart,mu,kepl)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: cart(6)
    REAL(WP),INTENT(in) :: mu
    REAL(WP),INTENT(out) :: kepl(6)

    REAL(kind=WP) :: x,y,z
    REAL(kind=WP) :: xp,yp,zp
    REAL(kind=WP) :: a,e,gom,pom,anom
    REAL(kind=WP) :: R
    REAL(kind=WP) :: USA, PUSA
    REAL(kind=WP) :: PN
    REAL(kind=WP) :: C1, C2, C3
    REAL(kind=WP) :: W1, W3, W4
    REAL(kind=WP) :: RCAR, CCAR
    REAL(kind=WP) :: UH
    REAL(kind=WP) :: HI, CHI, SINHI
    REAL(kind=WP) :: PP
    REAL(kind=WP) :: TGBETA
    REAL(kind=WP) :: U, V, ECOSU, ESINU
    REAL(kind=WP) :: CIN, QOS
    REAL(kind=WP) :: VPPOM
    INTEGER(kind=WP) :: IIE

    !********************
    !  LA SUBROUTINE
    !********************

    x  = cart(1)
    y  = cart(2)
    z  = cart(3)
    xp = cart(4)
    yp = cart(5)
    zp = cart(6)  

    R   = SQRT(X*X+Y*Y+Z*Z)
    USA = 2._DP/R-(XP*XP+YP*YP+ZP*ZP)/earth_mu
    a   = 1._DP/USA
    PN  = USA*SQRT(earth_mu*USA)

    C1   = X*YP - Y*XP
    C2   = Y*ZP - Z*YP
    C3   = X*ZP - Z*XP
    W4   = C2*C2 + C3*C3
    CCAR = W4 + C1*C1
    RCAR = SQRT(CCAR)

    UH = 1._DP/RCAR
    IF (W4.GT.CCAR*1.E-12) THEN
      CHI   = C1*UH
      SINHI = SQRT(1._DP - CHI*CHI)
      HI    = ACOS(CHI)
      GOM = ATAN2(C2,C3)
      GOM = MOD(GOM+2.*pi,2.*pi)      
    ELSE
      HI   = 0._WP
      SINHI= 0._WP
      IF (C1.LT.0._WP) HI = PI
      GOM=0._DP
    END IF

    PP = CCAR/earth_mu
    TGBETA = (X*XP + Y*YP + Z*ZP) * UH
    PUSA = PP*USA
    e = SQRT(1._WP-PUSA)

    !On limite la valeur de e a 10e-6

    IIE = INT(E * 1.E+06_WP)
    IF (IIE==0) e=0._WP
      
      
    ESINU = TGBETA*SQRT(PUSA)
    ECOSU = 1._WP - R*USA
    U = ATAN2(ESINU, ECOSU)
    ANOM = U - ESINU
    ANOM = MOD(ANOM+2.*pi,2.*pi)

    W1  = PP-R
    CIN = PP * TGBETA
    V   = ATAN2(CIN,W1)

    W3    = X*COS(GOM)+ Y*SIN(GOM)
    QOS   = W3*SINHI
    VPPOM = ATAN2(Z,QOS)
    
    IF (IIE == 0) THEN
      POM  = 0._WP
      ANOM = VPPOM
    ELSE
      POM = VPPOM-V
      POM = MOD(POM+2.*pi,2.*pi)
    END IF
    
    kepl(1) = A
    kepl(2) = E
    kepl(3) = HI
    kepl(4) = MOD(GOM+2.*pi,2.*pi)
    kepl(5) = MOD(POM+2.*pi,2.*pi)
    kepl(6) = MOD(ANOM+2.*pi,2.*pi)

  END SUBROUTINE cart_2_kepl

  !----------------------------------------------------------------------------!
  !> @brief Give the keplerian coordinates from cartesian coordinates
  !> @param[in] kepl  keplerian coordinates                                
  !> @param[in] mu    G*M                                                  
  !> @param[out] cart cartesian coordinates                                
  !----------------------------------------------------------------------------!
  SUBROUTINE kepl_2_cart(kepl,mu,cart)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: kepl(6)
    REAL(WP),INTENT(in) :: mu
    REAL(WP),INTENT(out) :: cart(6)

    REAL(WP) :: ea
    REAL(WP) :: cai,sai,cgo,sgo,cpo,spo,cae,sae
    REAL(WP) :: cgocpo,sgocpo,sgospo,cgospo
    REAL(WP) :: v1,v2,rld0,rmu0,sqmusa,rnu0,rki0
    REAL(WP) :: p(3),q(3)
    
    !********************
    !  LA SUBROUTINE
    !********************

    IF (kepl(2).lt.0) THEN

      STOP 'Error (kepl_2_cart subroutine) : the eccentricity can not be negative'

    ELSE IF (kepl(2).ge.1) THEN
      
      STOP 'Error (kepl_2_cart subroutine) : hyperbolic case'

    ELSE
 
      CALL ma_2_ea(kepl(6),kepl(2),ea)
    
      cai = COS(kepl(3))       
      sai = SIN(kepl(3))       
      cgo = COS(kepl(4))       
      sgo = SIN(kepl(4))           
      cpo = COS(kepl(5))       
      spo = SIN(kepl(5))       
      cae = COS(ea)     
      sae = SIN(ea)     

      v1 = SQRT(1.0-(kepl(2)**2)) 
      v2 = 1.0-(kepl(2)*cae)

      p(1) = cgo*cpo-cai*sgo*spo   
      p(2) = sgo*cpo+cai*cgo*spo   
      p(3) = SQRT(1.0-cai*cai)*spo
           
      q(1) = -cgo*spo-cai*sgo*cpo 
      q(2) = -sgo*spo+cai*cgo*cpo 
      q(3) = SQRT(1.0-cai*cai)*cpo

      rld0 = cae-kepl(2)   
      rmu0 = sae*v1         
      sqmusa = SQRT(mu/kepl(1))
      rnu0   = -sae/v2   
      rki0   = (cae*v1)/v2      
      
      cart(1:3) = kepl(1)*((rld0*p)+(rmu0*q))     
      cart(4:6) = sqmusa*((rnu0*p)+(rki0*q))   
      
    END IF

  END SUBROUTINE kepl_2_cart  

  !----------------------------------------------------------------------------!
  !> @brief Give the spherical coordinate from cartesian coordinates
  !> @param[in] cart  cartesian coordinates                         
  !> @param[out] sph  spherical coordinates                         
  !----------------------------------------------------------------------------!
  SUBROUTINE cart_2_sph(cart,sph)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: cart(6)
    REAL(WP),INTENT(out) :: sph(6)
    
    !********************
    !  LA SUBROUTINE
    !********************

    sph(1) = SQRT(cart(1)**2.0+cart(2)**2.0+cart(3)**2.0)
    sph(2) = MOD(ATAN2(cart(2),cart(1)),2*PI)
    sph(3) = ASIN(cart(3)/sph(1))
    sph(4) = (cart(1)*cart(4)+cart(2)*cart(5)+cart(3)*cart(6))/sph(1)
    sph(5) = (cart(1)*cart(5)-cart(4)*cart(2))/(cart(1)**2.0+cart(2)**2.0) 
    sph(6) = (cart(6)*(cart(1)**2.0+cart(2)**2.0)- &
         cart(3)*(cart(1)*cart(4)+cart(2)*cart(5)))/(SQRT(cart(1)**2.0+ &
         cart(2)**2.0)*(cart(1)**2.0+cart(2)**2.0+cart(3)**2.0))
    
  END SUBROUTINE cart_2_sph

  !----------------------------------------------------------------------------!
  !> @brief Give the cartesian coordinates from spherical coordinate
  !> @param[in] cart  cartesian coordinates                        
  !> @param[out] sph  spherical coordinates                        
  !----------------------------------------------------------------------------!
  SUBROUTINE sph_2_cart(sph,cart)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: sph(6)
    REAL(WP),INTENT(out) :: cart(6)
    
    !********************
    !  LA SUBROUTINE
    !********************

    cart(1) = sph(1)*cos(sph(3))*cos(sph(2)) 
    cart(2) = sph(1)*cos(sph(3))*sin(sph(2))
    cart(3) = sph(1)*sin(sph(3))             
    cart(4) = sph(4)*(cos(sph(3))*cos(sph(2)))- &
         sph(1)*(sph(6)*sin(sph(3))*cos(sph(2))+sph(5)*cos(sph(3))*sin(sph(2)))
    cart(5) = sph(4)*(cos(sph(3))*sin(sph(2)))+ &
         sph(1)*(-sph(6)*sin(sph(3))*sin(sph(2))+sph(5)*cos(sph(3))*cos(sph(2)))
    cart(6) = sph(4)*sin(sph(3))+sph(1)*sph(6)*cos(sph(3))

  END SUBROUTINE sph_2_cart

  !----------------------------------------------------------------------------!
  !> @brief Give the equinoctial coordinates from keplerian coordinate
  !> @param[in] kepl      keplerian coordinates                          
  !> @param[out] equinoc  equinoctial coordinates                    
  !----------------------------------------------------------------------------!
  SUBROUTINE kepl_2_equinoc(kepl,equinoc)

    !********************
    !   VARIABLES
    !********************

    REAL(WP),INTENT(in) :: kepl(6)
    REAL(WP),INTENT(out) :: equinoc(6)
    
    !********************
    !  LA SUBROUTINE
    !********************

    equinoc(1) = kepl(1)                                   
    equinoc(2) = kepl(2)*COS(kepl(4)+kepl(5))
    equinoc(3) = kepl(2)*COS(kepl(4)+kepl(5))
    equinoc(4) = kepl(6)+kepl(5)+kepl(4)
    equinoc(5) = SIN(kepl(3))*SIN(kepl(4))/(1+COS(kepl(3)))
    equinoc(6) = SIN(kepl(3))*COS(kepl(4))/(1+COS(kepl(3)))

  END SUBROUTINE kepl_2_equinoc 

  !----------------------------------------------------------------------------!
  !> @brief Function calculating the cross product between two vectors
  !> @param[in] a  vector 1                                                   
  !> @param[in] b  vector 2                                                   
  !----------------------------------------------------------------------------!
  FUNCTION cross_product(a, b) result(c)

    REAL(WP), DIMENSION(3), INTENT(IN) :: a, b
    REAL(WP),  DIMENSION(3) :: c

    c(1) = a(2) * b(3) - a(3) * b(2)
    c(2) = a(3) * b(1) - a(1) * b(3)
    c(3) = a(1) * b(2) - a(2) * b(1)

  END FUNCTION cross_product
  
END MODULE coordinates

