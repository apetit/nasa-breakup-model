!==============================================================================!
! UNAMUR, NASA Breakup model of EVOLVE 4.0                                     !
!==============================================================================!
! MODULE: breakupModel                                                         
!                                                                              
!> @file breakupModel.f90                                                      
!> @author Alexis Petit, PhD Student, University of Namur                      
!> @contact alexis.petit@obspm.fr                                              
!> @brief This module create a cloud of space debris.                          
!                                                                              
!==============================================================================!

MODULE breakupModel

  !============================================================================!
  !MODULE contains all subroutine about the date                               !
  !                                                                            !
  ! - parametersBreakup                                                        !
  ! - generateRandom                                                           !
  ! - fragmentation                                                            !
  ! - generateDebrisSC                                                         !
  ! - generateDebrisUS                                                         !
  ! - distriNormale                                                            !
  ! - distriAMbSup11UpperStage                                                 !
  ! - distriAMbSup11Spacecraft                                                 !
  ! - distriAMbInf8                                                            !
  ! - distriDeltaVExpl                                                         !
  ! - distriDeltaVColl                                                         !
  !============================================================================!

  USE constants 
  !USE IFPORT

  IMPLICIT NONE

  INTEGER :: nb_call 
  
  !Parameters of the NASA Breakup Model
  
  !Minimal and maximal ratio aera over mass [m2.kg-1]
  REAL(WP) :: am_min
  REAL(WP) :: am_max
  
  !Minimal and maximal size [m]
  REAL(WP):: size_max
  REAL(WP) :: size_min
  !Maximal increment velocity [m.s-1]
  REAL(WP) :: dv_max
  
  !Explosion
  REAL(WP) :: sigma_exp
  REAL(WP) :: mu_a_exp
  REAL(WP) :: mu_b_exp

  !Collision
  REAL(WP) :: sigma_coll
  REAL(WP) :: mu_a_coll
  REAL(WP) :: mu_b_coll

  !Spacecraft 
  REAL(WP) :: alpha_sc_1,alpha_sc_2_a,alpha_sc_2_b,alpha_sc_2_c,alpha_sc_3
  REAL(WP) :: mu1_sc_1,mu1_sc_2_a,mu1_sc_2_b,mu1_sc_2_c,mu1_sc_3
  REAL(WP) :: mu2_sc_1,mu2_sc_2_a,mu2_sc_2_b,mu2_sc_2_c,mu2_sc_3
  REAL(WP) :: sigma1_sc_1,sigma1_sc_2_a,sigma1_sc_2_b,sigma1_sc_2_c,sigma1_sc_3
  REAL(WP) :: sigma2_sc_1,sigma2_sc_2_a,sigma2_sc_2_b,sigma2_sc_2_c,sigma2_sc_3

  !Rocket body 
  REAL(WP) :: alpha_rb_1,alpha_rb_2_a,alpha_rb_2_b,alpha_rb_2_c,alpha_rb_3
  REAL(WP) :: mu1_rb_1,mu1_rb_2_a,mu1_rb_2_b,mu1_rb_2_c,mu1_rb_3
  REAL(WP) :: mu2_rb
  REAL(WP) :: sigma1_rb
  REAL(WP) :: sigma2_rb_1,sigma2_rb_2_a,sigma2_rb_2_b,sigma2_rb_2_c,sigma2_rb_3
  
CONTAINS

  !============================================================================!
  !> @brief Read the parameters of the breakup model                           
  !> @param[in] type_frag  type of fragmentation (R/B, S/C, or collision)      
  !> @param[in] name_file  name of the file containing the NBM parameters      
  !============================================================================!  
  SUBROUTINE parametersBreakup(type_frag,name_file)

    IMPLICIT NONE

    !********************!
    !   VARIABLES        !
    !********************!

    !Input/ouput variables 
    !------------------------------
    
    CHARACTER(20),INTENT(in) :: type_frag
    CHARACTER(50),INTENT(in) :: name_file

    !Internal variables
    !------------------------------    

    INTEGER :: error

    !********************!
    !   SUBROUTINES      !
    !********************!
    
    IF ("explosion".eq.type_frag) THEN

      OPEN(unit=21,file=TRIM(name_file),action="read",status="old",iostat=error)
      IF (error>0) THEN
        WRITE(*,*) 'Bad name for the NBM parameters file'
        STOP
      END IF
      
      READ(21,*)
      READ(21,*) am_min,am_max,size_min,size_max,dv_max
      READ(21,*)
      READ(21,*) alpha_sc_1,alpha_sc_2_a,alpha_sc_2_b,alpha_sc_2_c,alpha_sc_3
      READ(21,*)
      READ(21,*) mu1_sc_1,mu1_sc_2_a,mu1_sc_2_b,mu1_sc_2_c, mu1_sc_3, mu2_sc_1,&
                 mu2_sc_2_a,mu2_sc_2_b,mu2_sc_2_c,mu2_sc_3 
      READ(21,*)
      READ(21,*) sigma1_sc_1,sigma1_sc_2_a,sigma1_sc_2_b,sigma1_sc_2_c, &
                 sigma1_sc_3,sigma2_sc_1,sigma2_sc_2_a,sigma2_sc_2_b, &
                 sigma2_sc_2_c,sigma2_sc_3 
      READ(21,*)
      READ(21,*) sigma_exp, mu_a_exp, mu_b_exp            
      CLOSE(21)

    ELSE IF ("rocket_body".eq.type_frag) THEN

      OPEN(unit=21,file=TRIM(name_file),action="read",status="old",iostat=error)
      IF (error>0) THEN
        WRITE(*,*) 'Bad name for the NBM parameters file'
        STOP
      END IF
     
      READ(21,*)
      READ(21,*) am_min,am_max,size_min,size_max,dv_max
      READ(21,*)
      READ(21,*) alpha_rb_1,alpha_rb_2_a,alpha_rb_2_b,alpha_rb_2_c,alpha_rb_3
      READ(21,*)
      READ(21,*) mu1_rb_1,mu1_rb_2_a,mu1_rb_2_b,mu1_rb_2_c,mu1_rb_3,mu2_rb
      READ(21,*)
      READ(21,*) sigma1_rb,sigma2_rb_1,sigma2_rb_2_a,sigma2_rb_2_b,sigma2_rb_2_c,sigma2_rb_3 
      READ(21,*)
      READ(21,*) sigma_exp,mu_a_exp,mu_b_exp         
      CLOSE(21)

    ELSE

      WRITE(*,*) 'Bad type of fragmentation = ',type_frag
      STOP 

    END IF
        
  END SUBROUTINE parametersBreakup
  
  !============================================================================!
  !> @brief Generate a random number
  !============================================================================!
  SUBROUTINE generateRandom()

    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************

    INTEGER :: i,n,clock
    INTEGER, DIMENSION(:), ALLOCATABLE :: seed
   
    !********************!
    !   SUBROUTINES      !
    !********************!
          
    CALL RANDOM_SEED(size=n)
    ALLOCATE(seed(n))
    CALL SYSTEM_CLOCK(COUNT=clock)
    clock = 772736160
    seed=clock+37*(/(i-1,i=1,n)/)
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

  END SUBROUTINE generateRandom

  !============================================================================!
  !> @brief Create a population of space debris stored in an array         
  !> @param[in] type_frag: type of fragmentation (R/B, S/C, or collision)
  !> @param[in] name_file: name of the file containing the NBM parameters
  !> @param[in] s_scale: the scale factor                                
  !> @param[in] mass_sat: the mass of the satellite                      
  !> @param[out] nb: the number of fragments                             
  !> @param[out] population: an array storing the fragments of the cloud 
  !> @param[out] total_mass: the total mass of the fragments             
  !> @param[out] limit_size the minimal size of the objects created      
  !============================================================================!  
  SUBROUTINE fragmentation(type_frag,name_file,s_scale,mass_sat,nb,population,total_mass,limit_size)

    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************
    
    !Input/ouput variables 
    !------------------------------
    
    CHARACTER(20),INTENT(in) :: type_frag
    CHARACTER(50),INTENT(in) :: name_file
    REAL(WP), INTENT(in) :: s_scale
    REAL(WP), INTENT(in) :: mass_sat
    INTEGER, INTENT(out) :: nb
    REAL(WP), INTENT(out) :: population(1000000,6)  
    REAL(WP), INTENT(out) :: total_mass  
    REAL(WP), INTENT(out) :: limit_size
    
    !Internal variables
    !------------------------------
     
    !Current size
    REAL(WP) :: size_current
    !First estimation of the fragment number
    INTEGER :: nb_estimation
    !Counter of fragments
    INTEGER :: fragment_counter
    !Loop counter
    INTEGER :: i
    !Debris parameters
    REAL(WP) :: debris(6)
    !A random numbers
    REAL(WP) :: x,y
    !Step of the sampling
    INTEGER, PARAMETER :: nb_step = 100
    !Intermediate value to compute the probability law
    REAL(WP) :: n1,n2,step
    !Values of the probability law    
    REAL(WP) :: p_size_values(nb_step)
    !Size values of the cumulative function    
    REAL(WP) :: discretization(nb_step)
    !Area of the cumulative function
    REAL(WP) :: area_values(nb_step)

    !********************!
    !   SUBROUTINES      !
    !********************!
    
    CALL parametersBreakup(type_frag,name_file)
    CALL generateRandom()
    
    total_mass = 0    
    nb = 0
    nb_call = 0

    limit_size = size_min

    nb_estimation = int(s_scale*6*size_min**(-1.6))

    !Compute the area of the distribution law (following N(Lc) = S*Lc^{-1.6})
    OPEN(unit=18,file='outputs/size_proba_law.txt',action="write",status="replace")
    step = (LOG10(size_max)-LOG10(size_min))/nb_step
    DO i=1,nb_step
      n1 = s_scale*6*(10**(LOG10(size_min)+(i-1)*step))**(-1.6)
      n2 = s_scale*6*(10**(LOG10(size_min)+(i)*step))**(-1.6)
      p_size_values(i) = (n1-n2)/nb_estimation
      WRITE(18,*) LOG10(size_min)+(i-1)*step,p_size_values(i)
    END DO    
    CLOSE(18)

    !Compute the cumulative distribution function
    OPEN(unit=18,file='outputs/size_cumulative_distri.txt',action="write",status="replace")
    discretization(1) = LOG10(size_min)
    area_values(1)    = 0
    WRITE(18,*) discretization(1),area_values(1)     
    DO i=2,nb_step
      discretization(i) = LOG10(size_min)+(i-1)*step 
      area_values(i) = area_values(i-1) + step*p_size_values(i)
      WRITE(18,*) discretization(i),area_values(i) 
    END DO    
    CLOSE(18)    
    
    WRITE(*,'(A,I9,A,F6.3)') '| First estimation of the fragment number : ', &
                             nb_estimation,' with a minimal size of ',size_min

    DO WHILE (nb.lt.nb_estimation)

      !Compute the size (finding r that solve P(r) = y)   
      y = area_values(nb_step)*RAND()    
      i = 1
      DO WHILE(area_values(i)<y)
        i = i + 1
      END DO
      IF (i==nb_step) THEN 
        size_current = discretization(i)    
      ELSE
        size_current = discretization(i)+(discretization(i+1)-discretization(i))*RAND()
      END IF
      size_current = 10**size_current

      nb = nb + 1

      !Compute A/M, area, mass, and DV
      IF ((type_frag=='explosion').OR.(type_frag=='rocket_body')) THEN
        CALL generateFragment(type_frag,size_current,debris)
      ELSE IF (type_frag=='collision') THEN
        WRITE(*,*) 'Problem with the type of fragmentation (',type_frag,')'
        STOP
      END IF

      population(nb,1:6) = debris(1:6)
      total_mass = total_mass + population(nb,6)

    END DO

    IF (total_mass<mass_sat) THEN

      WRITE(*,'(A,F10.5,A,F10.5,A)') '| Warning: the mass is too small (', &
                                     total_mass,'<',mass_sat,')'

      ! We add the parent body less the mass of the fragments
      nb = nb + 1
      !population(nb,1)=1.
      !population(nb,2)=0.5
      !population(nb,3)=0.
      !population(nb,4)=0.
      !population(nb,5)=0.
      !population(nb,6)=mass_sat-total_mass

    ELSE IF (total_mass>mass_sat) THEN

      WRITE(*,'(A,F10.5,A,F10.5,A)') '| Warning: the mass is too big (', &
                                     total_mass,'>',mass_sat,')'

      ! We remove some fragments

      DO WHILE (total_mass>mass_sat)

        CALL RANDOM_NUMBER(x)         
        i = INT(nb*x)
        total_mass = total_mass - population(i,6) 
        population(i,1:6) = 0.
         
      END DO
      
    END IF

100 CONTINUE
  
  END SUBROUTINE fragmentation

  !============================================================================!
  !> @brief Distribution law of  A/M
  !> @param[in] type_frag    type of fragmentation       
  !> @param[in] sizeCurrent  size of the fragment       
  !> @param[in] am           ratio area-to-mass       
  !> @param[out] distri_am   probability of a A/M
  !============================================================================!
  SUBROUTINE compute_am_distribution_value(type_frag,size_current,am,distri_am)
    
    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************
    
    !Input/ouput variables 
    !------------------------------

    CHARACTER(20),INTENT(IN) :: type_frag    
    REAL(WP),INTENT(IN) :: size_current,am
    REAL(WP),INTENT(OUT) :: distri_am

    !Internal variables
    !------------------------------    

    !Value of the bridging function
    REAL(WP) :: BF
    !A random numbers
    REAL(WP) :: R

    !********************!
    !   SUBROUTINES      !
    !********************!

    IF(size_current.gt.0.11) THEN
      SELECT CASE(type_frag)
        CASE('explosion')
          CALL distriAMbSup11Spacecraft(log10(size_current),log10(am),distri_am)
        CASE('rocket_body')
          CALL distriAMbSup11UpperStage(log10(size_current),log10(am),distri_am)
        CASE DEFAULT
          WRITE(*,*) 'Problem with the type of fragmentation (',type_frag,')'
          STOP
       END SELECT  
    ELSE IF(size_current.lt.0.08) THEN
      CALL distriAMbInf8(log10(size_current),log10(am),distri_am)
    ELSE
      BF = 10*(LOG10(size_current) + 1.105)
      CALL RANDOM_NUMBER(R)
      IF (R.gt.BF) THEN
        SELECT CASE(type_frag)
          CASE('explosion')
          CALL distriAMbSup11Spacecraft(log10(size_current),log10(am),distri_am)
        CASE('rocket_body')
          CALL distriAMbSup11UpperStage(log10(size_current),log10(am),distri_am)
        CASE DEFAULT
          WRITE(*,*) 'Problem with the type of fragmentation (',type_frag,')'
          STOP
        END SELECT
      ELSE
        CALL distriAMbInf8(log10(size_current),log10(am),distri_am)
      END IF
    END IF
   
    IF (isnan(distri_am)) THEN
      distri_am = 0.0
    END IF
    
  END SUBROUTINE compute_am_distribution_value
   
  !============================================================================!
  !> @brief Create a fragment for a collision or an explosion
  !> @param[in] type_frag    type of fragmentation                             
  !> @param[in] sizeCurrent  size of the fragment                             
  !> @param[out] debris      characteristics of the fragment               
  !============================================================================!
  SUBROUTINE generateFragment(type_frag,size_current,debris)

    IMPLICIT NONE

    !********************
    !   VARIABLES
    !********************
    
    !Input/ouput variables 
    !------------------------------

    CHARACTER(20),INTENT(IN) :: type_frag 
    REAL(WP),INTENT(IN) :: size_current
    REAL(WP),INTENT(OUT) :: debris(6)    

    !Internal variables
    !------------------------------

    !Mass of a space debris
    REAL(WP) :: mass
    !Ratio area over mass of a space debris
    REAL(WP) :: am
    !Area of a space debris
    REAL(WP) :: area
    !Longitude et latitude of the direction of the increment of velocity
    REAL(WP) :: phi,lambda
    !Maximal and current increment velocity
    REAL(WP) :: dv,dvx,dvy,dvz
    !Probabilities of the ratio A/M and module of the velocity increment
    REAL(WP) :: distri_dv
    !Random numbers
    REAL(WP) :: x,y
    !Step of the sampling
    REAL(WP) :: step
    !Probabilities to obtain the ratio A/M
    Real(WP) :: last_p_am,p_am
    !Number of step
    INTEGER, PARAMETER :: nb_step = 500
    !Antecedants of the distribution    
    REAL(WP), DIMENSION(1:nb_step) :: discretization
    !Area of the distribution
    REAL(WP), DIMENSION(1:nb_step) :: area_values
    !Loop counter
    INTEGER :: i

    !********************!
    !   SUBROUTINES      !
    !********************!
    
    !Compute the area of the distribution law
    step              = ABS(LOG10(am_max)-LOG10(am_min))/nb_step
    discretization(1) = LOG10(am_min)
    area_values(1)    = 0
    CALL compute_am_distribution_value(type_frag,size_current,10*discretization(1),last_p_am)
    IF (nb_call==0) THEN
      OPEN(unit=18,file='outputs/am_proba_law.txt',action="write",status="replace")
    ELSE
      OPEN(unit=18,file='outputs/am_proba_law.txt',status="old", position="append", action="write")
    END IF
    nb_call = nb_call+1
    WRITE(18,*) nb_call,discretization(1),last_p_am,area_values(1)
    DO i=2,nb_step
        discretization(i) = discretization(i-1) + step
        CALL compute_am_distribution_value(type_frag,size_current,10**discretization(i),p_am)
        area_values(i) = area_values(i-1) + step*MAX(last_p_am,p_am)
        WRITE(18,*) nb_call,discretization(i),last_p_am,area_values(i)
        last_p_am = p_am
    END DO
    CLOSE(18) 

    !Find r that solve P(r) = y   
    y = area_values(nb_step)*RAND()    
    i = 1
    DO WHILE(area_values(i)<y)
      i = i + 1
    END DO
    IF (i==nb_step) then 
       am = 10**discretization(i)    
     ELSE
       am = 10**((discretization(i)+discretization(i+1))/2)
    END IF
   
    !Compute the area and the mass
    IF(size_current.lt.0.00167) THEN
      area=0.540424*size_current**2
    ELSE
      area=0.556945*size_current**2.0047077
    END IF      
    mass=area/am

    !Compute the increment velocity of a fragment
    CALL RANDOM_NUMBER(x)
    dv=x*dv_max
    CALL distriDeltaVExpl(log10(dv),log10(am),distri_dv)
    CALL RANDOM_NUMBER(y)
    DO WHILE(y.gt.distri_dv) 
      CALL RANDOM_NUMBER(x)
      dv=x*dv_max
      CALL distriDeltaVExpl(log10(dv),log10(am),distri_dv)
      CALL RANDOM_NUMBER(y)
    END DO

    !Compute the direction of the increment velocity of a fragment
    CALL RANDOM_NUMBER(x)
    phi=x*2*PI-PI
    CALL RANDOM_NUMBER(x)
    lambda=x*PI-PI/2.
    dvx=dv*cos(phi)*cos(lambda)
    dvy=dv*cos(phi)*sin(lambda)
    dvz=dv*sin(phi)
    
    !Save the debris
    debris(1)=size_current
    debris(2)=am
    debris(3)=dvx
    debris(4)=dvy
    debris(5)=dvz
    debris(6)=mass

  END SUBROUTINE generateFragment

  !============================================================================!
  !> @brief Normal law                                 
  !> @param[in] mu     mean
  !> @param[in] sigma  standard deviation
  !> @param[in] x      variable
  !> @param[out] p     probability density
  !============================================================================!  
  SUBROUTINE distriNormale(mu,sigma,x,p)

    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(IN) :: mu
    REAL(WP),INTENT(IN) :: sigma
    REAL(WP),INTENT(IN) :: x
    REAL(WP),INTENT(OUT) :: p

    !********************!
    !   SUBROUTINES      !
    !********************!

    p = (1/(sigma*SQRT(2*PI))*EXP(-1/2.*((x-mu)/sigma)**2))

  END SUBROUTINE distriNormale

  !============================================================================!
  !> @brief Distribution law of A/M for fragments > 11 cm (rocket-body, eq. 5)
  !> @param[in] lambda log(Lc)                                         
  !> @param[in] chi    log(A/M)                                          
  !> @param[out] DAM   probability of a A/M    
  !============================================================================!
  SUBROUTINE distriAMbSup11UpperStage(lambda,chi,DAM)
    
    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(in) :: lambda
    REAL(WP),INTENT(in) :: chi
    REAL(WP),INTENT(out) :: DAM

    !Internal variables
    !------------------------------

    REAL(WP) :: alpha
    REAL(WP) :: mu1
    REAL(WP) :: mu2
    REAL(WP) :: sigma1
    REAL(WP) :: sigma2
    REAL(WP) :: N1 
    REAL(WP) :: N2

    !********************!
    !   SUBROUTINES      !
    !********************!

    !calculation of alpha
    IF(lambda.le.-1.4) THEN
      alpha=alpha_rb_1
    ELSE IF((lambda.gt.-1.4).AND.(lambda.lt.0)) THEN
      alpha=alpha_rb_2_a+alpha_rb_2_b*(lambda+alpha_rb_2_c)
    ELSE
      alpha=alpha_rb_3
    END IF

    !calculation of mu1
    IF(lambda.le.-0.5) THEN
      mu1=mu1_rb_1
    ELSE IF((lambda.gt.-0.5).AND.(lambda.lt.0)) THEN
      mu1=mu1_rb_2_a+mu1_rb_2_b*(lambda+mu1_rb_2_c)
    ELSE
      mu1=mu1_rb_3   
    END IF

    !calculation of mu2
    mu2=mu2_rb 

    !calculation of sigma1
    sigma1=sigma1_rb

    !calculation of sigma2
    IF(lambda.le.-1.0) THEN
      sigma2=sigma2_rb_1
    ELSE IF((lambda.gt.-1.0).AND.(lambda.lt.0.1)) THEN
      sigma2=sigma2_rb_2_a+sigma2_rb_2_b*(lambda+sigma2_rb_2_c)
    ELSE
      sigma2=sigma2_rb_3
    END IF

    !calculation of the normal law
    CALL distriNormale(mu1,sigma1,chi,N1)
    CALL distriNormale(mu2,sigma2,chi,N2)
  
    !calculation of the propability of A/M
    DAM=alpha*N1*sigma1+(1-alpha)*N2*sigma2

  END SUBROUTINE distriAMbSup11UpperStage

  !============================================================================!
  !> @brief Distribution law of A/M for fragments > 11 cm (spacecraft, eq. 6)  
  !> @param[in] lambda log(Lc)                                         
  !> @param[in] chi    log(A/M)                                          
  !> @param[out] DAM   probability of a A/M   
  !============================================================================!  
  SUBROUTINE distriAMbSup11Spacecraft(lambda,chi,DAM)
    
    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(IN) :: lambda
    REAL(WP),INTENT(IN) :: chi
    REAL(WP),INTENT(OUT) :: DAM

    !Internal variables
    !------------------------------

    REAL(WP) :: alpha
    REAL(WP) :: mu1
    REAL(WP) :: mu2
    REAL(WP) :: sigma1
    REAL(WP) :: sigma2
    REAL(WP) :: N1 
    REAL(WP) :: N2

    !********************!
    !   SUBROUTINES      !
    !********************!
  
    !calculation of alpha
    IF(lambda.le.-1.95) THEN
      alpha=alpha_sc_1
    ELSE IF((lambda.gt.-1.95).AND.(lambda.lt.0.55)) THEN
      alpha=alpha_sc_2_a+alpha_sc_2_b*(lambda+alpha_sc_2_c)
    ELSE
      alpha=alpha_sc_3
    END IF

    !calculation of mu1
    IF(lambda.le.-1.1) THEN
      mu1=mu1_sc_1
    ELSE IF((lambda.gt.-1.1).AND.(lambda.lt.0)) THEN
      mu1=mu1_sc_2_a+mu1_sc_2_b*(lambda+mu1_sc_2_c)
    ELSE
      mu1=mu1_sc_3   
    END IF

    !calculation of mu2
    IF(lambda.le.-0.7) THEN
      mu2=mu2_sc_1
    ELSE IF((lambda.gt.-0.7).AND.(lambda.lt.-0.1)) THEN
      mu2=mu2_sc_2_a+mu2_sc_2_b*(lambda+mu2_sc_2_c)
    ELSE
      mu2=mu2_sc_3   
    END IF
    
    !calculation of sigma1
    IF(lambda.le.-1.3) THEN
      sigma1=sigma1_sc_1
    ELSE IF((lambda.gt.-1.3).AND.(lambda.lt.-0.3)) THEN
      sigma1=sigma1_sc_2_a+sigma1_sc_2_b*(lambda+sigma1_sc_2_c)
    ELSE
      sigma1=sigma1_sc_3
    END IF

    !calculation of sigma2
    IF(lambda.le.-0.5) THEN
      sigma2=sigma2_sc_1
    ELSE IF((lambda.gt.-0.5).AND.(lambda.lt.-0.3)) THEN
      sigma2=sigma2_sc_2_a+sigma2_sc_2_b*(lambda+sigma2_sc_2_c)
    ELSE
      sigma2=sigma2_sc_3
    END IF

    !calculation of the normal law
    CALL distriNormale(mu1,sigma1,chi,N1)
    CALL distriNormale(mu2,sigma2,chi,N2)
  
    !calculation of the probability of A/M
    DAM=alpha*N1*sigma1+(1-alpha)*N2*sigma2

  END SUBROUTINE distriAMbSup11Spacecraft

  !============================================================================!
  !> @brief Distribution law of A/M for objects < 8 cm (eq. 7)          
  !> @param[in] lambda log(Lc)                                         
  !> @param[in] chi    log(A/M)                                          
  !> @param[out] DAM   probability of a A/M 
  !============================================================================!  
  SUBROUTINE distriAMbInf8(lambda,chi,DAM)

    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(IN) :: lambda
    REAL(WP),INTENT(IN) :: chi
    REAL(WP),INTENT(OUT) :: DAM

    !Internal variables
    !------------------------------

    REAL(WP) :: mu
    REAL(WP) :: sigma

    !********************!
    !   SUBROUTINES      !
    !********************!

    !calculation of mu
    IF(lambda.le.-1.75) THEN
      mu=-0.30
    ELSE IF((lambda.gt.-1.75).AND.(lambda.lt.-1.25)) THEN
      mu=-0.3-1.4*(lambda+1.75)
    ELSE
      mu=-1.00    
    END IF

    !calculation of sigma
    IF(lambda.le.-3.5) THEN
      sigma=0.2
    ELSE
      sigma=0.2+0.1333*(lambda+3.5)
    END IF

    !calculation of the probability of A/M
    CALL distriNormale(mu,sigma,chi,DAM)

  END SUBROUTINE distriAMbInf8

  !============================================================================!
  !> @brief Distribution law of DV for explosion (eq. 11)  
  !> @param[in] nu   log(DV)                                           
  !> @param[in] chi  log(A/M)                                          
  !> @param[out] DdV probability of a DV                                    
  !============================================================================!  
  SUBROUTINE distriDeltaVExpl(nu,chi,DdV)

    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(IN) :: nu
    REAL(WP),INTENT(IN) :: chi
    REAL(WP),INTENT(OUT) :: DdV

    !Internal variables
    !------------------------------

    REAL(WP) :: mu
 
    !********************!
    !   SUBROUTINES      !
    !********************!
    
    mu = mu_a_exp*chi+mu_b_exp
    CALL distriNormale(mu,sigma_exp,nu,DdV)

  END SUBROUTINE distriDeltaVExpl

  !============================================================================!
  !> @brief Distribution law of DV for collision (eq. 12)    
  !> @param[in] nu   log(DV)                                      
  !> @param[in] chi  log(A/M)                                
  !> @param[out] DdV probability of a DV                    
  !============================================================================!  
  SUBROUTINE distriDeltaVColl(nu,chi,DdV)

    IMPLICIT NONE

    !Input/ouput variables 
    !------------------------------

    REAL(WP),INTENT(IN) :: nu
    REAL(WP),INTENT(IN) :: chi
    REAL(WP),INTENT(OUT) :: DdV

    !Internal variables
    !------------------------------

    REAL(WP) :: mu
 
    !********************!
    !   SUBROUTINES      !
    !********************!
    
    mu = mu_a_coll*chi+mu_b_coll
    CALL distriNormale(mu,sigma_coll,nu,DdV)

  END SUBROUTINE distriDeltaVColl

END MODULE breakupModel 

