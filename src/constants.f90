!==============================================================================!
! UNAMUR, NASA Breakup model of EVOLVE 4.0                                     !
!==============================================================================!
! MODULE: constants                                                         
!                                                                              
!> @file constants.f90                                                      
!> @author Alexis Petit, PhD Student, University of Namur                      
!> @contact alexis.petit@obspm.fr                                              
!> @brief This module contains all physical constants and types       
!                                                                              
!==============================================================================

MODULE constants

  IMPLICIT NONE

  !---------------------------------------------------------------------------!
  !
  ! Declaration of the constants
  !
  !---------------------------------------------------------------------------!
  
  !Symbolic names for kind types of 4-, 2-, and 1-byte integers:
  INTEGER, PARAMETER :: I4B = SELECTED_INT_KIND(9)
  INTEGER, PARAMETER :: I2B = SELECTED_INT_KIND(4)
  INTEGER, PARAMETER :: I1B = SELECTED_INT_KIND(2)

  !Symbolic names for kind types of single- and double-precision reals:
  INTEGER, PARAMETER :: SP = KIND(1.0)
  INTEGER, PARAMETER :: DP = KIND(1.0D0)
  INTEGER, PARAMETER :: WP=DP

  !Symbolic names for kind types of single- and double-precision complex:
  INTEGER, PARAMETER :: SPC = KIND((1.0,1.0))
  INTEGER, PARAMETER :: DPC = KIND((1.0D0,1.0D0))

  !Symbolic name for kind type of default logical:
  INTEGER, PARAMETER :: LGT = KIND(.TRUE.)

  !Frequently used mathematical constants (with precision to spare):
  REAL(SP), PARAMETER :: PI=3.141592653589793238462643383279502884197_sp
  REAL(SP), PARAMETER :: PIO2=1.57079632679489661923132169163975144209858_sp
  REAL(SP), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_sp
  REAL(SP), PARAMETER :: SQRT2=1.41421356237309504880168872420969807856967_sp
  REAL(SP), PARAMETER :: EULER=0.5772156649015328606065120900824024310422_sp
  REAL(DP), PARAMETER :: PI_D=3.141592653589793238462643383279502884197_dp
  REAL(DP), PARAMETER :: PIO2_D=1.57079632679489661923132169163975144209858_dp
  REAL(DP), PARAMETER :: TWOPI_D=6.283185307179586476925286766559005768394_dp
  REAL(WP), PARAMETER :: PI_W=3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170_wp
  REAL(WP), PARAMETER :: PIO2_W=1.57079632679489661923132169163975144209858_wp
  REAL(WP), PARAMETER :: TWOPI_W=6.283185307179586476925286766559005768394_wp

  !> @brief Gravitational constant \f$ m^3 s^{-2} kg^{-1}\f$
  REAL(WP),PARAMETER :: gravitational_constant = 6.6742867D-11
  !> @brief Astronomical unit (in meters)
  REAL(WP),PARAMETER :: au_mean = 149597870691.0_wp
  !> @brief Solar radiation at 1 UA (\f$N/m^2=kg/(m*s^2)\f$)
  REAL(WP),PARAMETER :: c_srp_mean = 4.56D-6
  !> @brief Equatorial radius of the Sun  
  REAL(WP) :: sun_req = 6.960D8
  !> @brief Equatorial radius of the Earth  
  REAL(WP), PARAMETER :: earth_eq = 6.3781366D6
  !> @brief Mass of the Earth  
  REAL(WP), PARAMETER :: earth_mass = 5.972D24
  !> @brief J2  
  REAL(WP), PARAMETER :: earth_J2 = 1.0826262207D-3
  !> @brief Earth mu
  REAL(WP), PARAMETER :: earth_mu = 0.3986004415E15
  !> @brief Mass of the Sun 
  REAL(WP), PARAMETER :: sun_mass = 1.9891E30
  !> @brief Mass of the Moon  
  REAL(WP), PARAMETER :: moon_mass = 7.34767309E22

  !---------------------------------------------------------------------------!
  !
  ! Declaration of the types
  !
  !---------------------------------------------------------------------------!
  
  TYPE sprs2_sp
     INTEGER(I4B) :: n,len
     REAL(SP), DIMENSION(:), POINTER :: val
     INTEGER(I4B), DIMENSION(:), POINTER :: irow
     INTEGER(I4B), DIMENSION(:), POINTER :: jcol
  END TYPE sprs2_sp

  TYPE sprs2_dp
     INTEGER(I4B) :: n,len
     REAL(DP), DIMENSION(:), POINTER :: val
     INTEGER(I4B), DIMENSION(:), POINTER :: irow
     INTEGER(I4B), DIMENSION(:), POINTER :: jcol
  END TYPE sprs2_dp

  TYPE satellite
     INTEGER :: id_norad
     REAL(WP) :: state_vector(6)
     REAL(WP) :: bc
     REAL(WP) :: amr
     REAL(WP) :: size
     CHARACTER(len=30) :: name
  END TYPE satellite

  TYPE observer_data
     REAL(WP) :: latitude
     REAL(WP) :: longitude
     REAL(WP) :: altitude
  END TYPE observer_data

  TYPE date_type
     INTEGER :: year
     INTEGER :: month
     INTEGER :: day
     INTEGER :: hour
     INTEGER :: minute
     REAL(WP) :: second
  END TYPE date_type  
  
  TYPE celestial_body
    CHARACTER(len=30) :: name
    REAL(WP) :: mu
    REAL(WP) :: Req 
    REAL(WP) :: omega
    REAL(WP) :: theta_init
    REAL(WP) :: kepl(6)
  END TYPE celestial_body  

  TYPE space_weather
    REAL(WP) :: jd
    REAL(WP) :: f107
    REAL(WP) :: f107m
    REAL(WP) :: mgII
    REAL(WP) :: ap_mean
    REAL(WP) :: ap(8)
    REAL(WP) :: ap_current
    REAL(WP) :: s10
    REAL(WP) :: s10m
    REAL(WP) :: xm10
    REAL(WP) :: xm10m
    REAL(WP) :: y10
    REAL(WP) :: y10m
 END TYPE space_weather
      
END MODULE constants

